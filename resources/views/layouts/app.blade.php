<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="shortcut icon" href="/uploads/favicon.png"/>
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title')</title>
        
        <link href="http://fonts.cdnfonts.com/css/montserrat" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <head>
    <body>
        @yield('content')
        <script src="{{ mix('js/app.js') }}"></script>
    </body>
</html>