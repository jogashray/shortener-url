
@extends('layouts.app')
@section("title", "Shortener URL")
@section('content')
@php
//$http = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http" ;
//$hm =  $http."://".$_SERVER['HTTP_HOST'];//" X " .$_SERVER['REQUEST_URI'];
@endphp
<div id="app">
	<div class="wrapper">
        <div class="topnav">
          <router-link :to="{name: 'Home_URL' }">Home</router-link >
          <router-link :to="{name: 'URL_List' }">URLs</router-link >
        </div>

        <div class="app_title">
            <span>{{ env('APP_NAME', null)}}</span>
        </div>
        <div class="content-wrapper">
			<router-view></router-view>
		</div>
	</div>
</div>
@endsection

