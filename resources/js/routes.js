
import URLComponent from "./components/UrlComponent.vue";
import FormComponent from "./components/FormComponent.vue";
export const routes = [
    {
        path: "/url-list",
        name: "URL_List",
        component: URLComponent
    },
    {
        path: "/",
        name: "Home_URL",
        component: FormComponent
    }
];