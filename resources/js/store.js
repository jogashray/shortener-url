import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex)

const store = new Vuex.Store({
	state:{
		successModal : false,
		successModalTitle :null,
		successModalMessage: null,

		alertModal :false,
		alertModalTitle: null,
		alertModalMessage: null,
		short_url : null,
	},
	mutations:{
		changeSuccessModalStatus(state, data={'title': null, 'message': null, 'short_url': null} ){
			state.successModal = !state.successModal ; 
			state.successModalTitle = data.title;
			state.successModalMessage = data.message ;
			state.short_url = data.short_url;
		},
		changeAlertModalStatus(state, data={'title': null, 'message': null}){
			state.alertModal = !state.alertModal ; 
			state.alertModalTitle = data.title;
			state.alertModalMessage = data.message ;
		}
	},
	actions:{
		successMessage({commit}, data){
			commit("changeSuccessModalStatus", data)
		},
		alertMessage({commit}, data){
			commit("changeAlertModalStatus", data);
		}
	}
})
export default store