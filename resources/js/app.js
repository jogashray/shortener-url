require('./bootstrap');

window.Vue = require('vue').default;
import Vue from "vue";
import VueRouter from "vue-router";
import { routes } from "./routes";
import VueClipboard from 'vue-clipboard2'
import store from './store';
import axios from "axios" ;

Vue.use(VueRouter);
Vue.use(VueClipboard)

const router = new VueRouter({
    mode: "history",
    routes: routes
});

const app = new Vue({
    el: '#app',
    store,
    router: router,
});
