<p align="center"><img src="cover.png" ></p>

## About Shortener URL Apps

Shortener URL Apps is a web-based software. This app converts long URL into short url. Whenever going to short url, it will redirect to main/original url. 

### How can used?
To use this app, go to the [link](https://tiny.alphatechbd.xyz/) .

### How does generate a short URL?
In this short URl, contains only alphanumeric characters. There are 62 character such as 0 to 9, a to z , A to Z.
If you need max 6 characters long URL, You can get almost 62^6 = 56 Billion URLs. Every short URL must be unique as this short URL generated depends on index id. You may check the [hash function](https://prnt.sc/6anXg6TcXJkQ) .

### Installation :
To run this project in live server, just configure env file & database connections.

## Caution 
You must set valid [APP_URL](https://prnt.sc/i-MixXMidS3L) in .evn file.

### Technologies :
[Laravel 8](https://laravel.com/docs/8.x/installation), [VueJS](https://vuejs.org/)