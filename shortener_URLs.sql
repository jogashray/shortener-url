-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 28, 2022 at 01:18 PM
-- Server version: 10.3.34-MariaDB
-- PHP Version: 7.3.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shortener_URLs`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2022_03_27_154654_create_shorteners_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `shorteners`
--

CREATE TABLE `shorteners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `hash_key` varchar(7) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `original_url` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shorteners`
--

INSERT INTO `shorteners` (`id`, `hash_key`, `original_url`, `created_at`, `updated_at`) VALUES
(80, 'i1', 'https://laravel.com/docs/8.x/configuration', '2022-03-28 19:52:18', '2022-03-28 19:52:18'),
(81, 'j1', 'https://laravel.com/docs/8.x/routing', '2022-03-28 19:52:38', '2022-03-28 19:52:38'),
(82, 'k1', 'https://vuejs.org/guide/introduction.html', '2022-03-28 19:53:01', '2022-03-28 19:53:01'),
(83, 'l1', 'https://vuejs.org/guide/essentials/template-syntax.html', '2022-03-28 19:53:16', '2022-03-28 19:53:16'),
(84, 'm1', 'http://tiny.alphatechbd.xyz', '2022-03-28 19:58:08', '2022-03-28 19:58:08'),
(85, 'n1', 'http://tiny.alphatechbd.xyz/m12', '2022-03-28 20:22:53', '2022-03-28 20:22:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shorteners`
--
ALTER TABLE `shorteners`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `shorteners_hash_key_unique` (`hash_key`),
  ADD KEY `shorteners_original_url_index` (`original_url`(768));

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `shorteners`
--
ALTER TABLE `shorteners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
