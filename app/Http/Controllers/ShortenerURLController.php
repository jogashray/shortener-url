<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Shortener ;
use App\Http\Helper;

class ShortenerURLController extends Controller
{
    use Helper ;
    public function redirect(Request $request, $hash_key){
        $original_url = $this->getOriginalURL($hash_key) ;
        if($original_url){
            return redirect( $original_url, 301); 
        }
        return redirect('/')->with('message', 'Kindly Generate valid URL.');
    }
}
