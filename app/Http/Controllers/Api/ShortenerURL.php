<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Shortener ;
use App\Http\Helper;
use Illuminate\Support\Facades\DB;

class ShortenerURL extends Controller
{
    public $APP_URL = null;
    use Helper ;
    public function __construct(){

        $this->ChangeAppURL();
        
    }
    public function index(Request $request){
        $urls = Shortener::latest()->take(100)->get();
        return response()->json(['data'=> $urls, 'app_url'=> $this->APP_URL]);
    }
    public function store(Request $request){
        $input = $request->only(['original_url']) ;

        if(!$this->isValidURL($input['original_url'])){
            return response()->json(['success'=> false, 'message'=> "Sorry. The URL is not valid." ]);
        }

        $safe_res_code = $this->isSafeURL($input['original_url']);

        if( $safe_res_code != 200){
            return response()->json(['success'=> false, 'message'=> "Sorry. The URL is not safe." ]);
        }

        $hasKey = $this->hasExistOrGet($input['original_url']);
        
        if($hasKey){
            $short_url = $this->APP_URL.$hasKey ;
            return response()->json(['success'=> true, 'short_url'=>$short_url, 'message'=>'Click below to copy the URL.' ]);
        }
        DB::beginTransaction();
        try {
            $data = Shortener::create($input) ;
            $response = $this->updateKey($data['id']);
            DB::commit();
            if($response['success']){
                $short_url = $this->APP_URL.$response['hash_key'] ;
                return response()->json(['success'=> true, 'short_url'=>$short_url, 'message'=>'Click below to copy the URL' ]);
            }
        }catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success'=> false, 'message'=> 'Something is wrong..' ]);
        }
        
    }
    public function show(Request $request){
        return ['message'=> 'Post it.. show'];
    }

    public function ChangeAppURL()
    {
        $url = env("APP_URL", null) ;
        if($url && substr($url, -1) != "/" ){
            $this->APP_URL = $url . "/" ;
        }else{
            $this->APP_URL = $url ;
        }
        
    }
}
