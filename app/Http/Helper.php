<?php
namespace App\Http;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\Shortener ;

trait Helper {

    
    // Generate hash key according to id
    public function getHash( $id ){
        //Don't change this value
        $alph_anum = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $hash_str = '';
        while($id > 0){
            $hash_str .= $alph_anum[ $id % 62] ;
            $id = floor($id / 62) ;
        }
        return $hash_str ;
    }

    // Update Hash-key
    public function updateKey($id){
        $shortener = Shortener::findOrFail($id) ;
        $hash_key = $this->getHash($id) ;

        if(strlen($hash_key) > 0){
            $shortener->hash_key = $hash_key ;
            $res = $shortener->save();
            if($res){
                return ['success' => true, 'message' => '', 'hash_key'=> $hash_key] ;
            }
        }
        return ['success' => false, 'message' => 'Wrong'] ;
        
    }

    //Check whether URL is safe or not
    // Return status code 200 if it is safe, else error.
    public function isSafeURL($url){
        $response = Http::post("https://safebrowsing.googleapis.com/v4/threatMatches:find?key=AIzaSyDtmGtyQejiuKZDQ2ZrRYjxK34F-1AaWd8" , [
                "client"=> [
                  "clientId"=>      "jogash_ray",
                  "clientVersion"=> "1.5.2"
                ],
                "threatInfo"=> [
                  "threatTypes"=>      ["MALWARE", "SOCIAL_ENGINEERING"],
                  "platformTypes"=>    ["WINDOWS"],
                  "threatEntryTypes"=> ["URL"],
                  "threatEntries"=> [
                    ["url"=> $url]
                  ]
                ]
            ]); 
        return $response->status() ? $response->status() : null;
    }

    // Check whether url exists or not, if exists, return hash-key
    public function hasExistOrGet($url){
        $query = Shortener::where('original_url', $url)->first();
        if($query && isset($query->hash_key) && $query->hash_key != null){
            return $query->hash_key ;
        }
        return false ;
    }

    // Get original url by hash key
    public function getOriginalURL($hash_key){
    	$query = Shortener::where('hash_key', $hash_key)->first();
        if($query && isset($query->original_url)){
            return $query->original_url ;
        }
        return false ;
    }

    // Check whether the url is valid or not
    public function isValidURL($url){
        if (filter_var($url, FILTER_VALIDATE_URL) === false) {
            return false;
        }
        return true;
    }
}